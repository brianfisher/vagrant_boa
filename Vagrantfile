#### commonly modified per user ################################################

# you can override anything in p.extra_vars below
PROVISIONER_VARS = {
  # git: {
  #   config: {
  #     user: [
  #       { option: 'name',             value: 'Brian Fisher' },
  #       { option: 'email',            value: 'me@example.com' },
  #     ],
  #   },
  # },
}

# virtual machine settings
VM_MEM = 1536
VM_CPU_NUM = 2

# If you run other vagrant machines, this needs to be unique per base box
VAGRANT_BOX = 'precise64'

# If you run other vagrant machines, you may want to place them on the same
# subnet, e.g. for my virtualbox machines I would use
# PROVIDER = 'virtualbox'
# CLIENT_IP = '192.168.34.18'
# HOST_IP = '192.168.34.1' # should be .1
# VAGRANT_BOX_URL = 'http://files.vagrantup.com/precise64.box'
# for my vmware_fusion machines
PROVIDER = 'vmware_fusion'
CLIENT_IP = '192.168.224.18'
HOST_IP = '192.168.224.1'
VAGRANT_BOX_URL = 'http://3c43fbb8407cb9867a15-9f663f63e7e4050b7617be92778af38f.r83.cf1.rackcdn.com/precise64-vmware-fusion602.box'

#### you shouldn't need to modify anything below ###############################

HOSTNAME = 'aegir.local'
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = VAGRANT_BOX
  config.vm.hostname = HOSTNAME
  config.vm.network :private_network, ip: CLIENT_IP
  config.ssh.forward_agent = true

  # disable default sharing
  config.vm.synced_folder ".", "/vagrant", disabled: true

  if PROVIDER == 'virtualbox'
    config.vm.provider :virtualbox do |v|
      v.memory = VM_MEM
      v.cpus = VM_CPU_NUM
      v.name = HOSTNAME
      config.vm.box_url = VAGRANT_BOX_URL
    end
  elsif PROVIDER == 'vmware_fusion'
    config.vm.provider :vmware_fusion do |v|
      v.vmx["memsize"] = VM_MEM
      v.vmx["numvcpus"] = VM_CPU_NUM
      v.vmx["displayName"] = HOSTNAME
      config.vm.box_url = VAGRANT_BOX_URL
    end
  end

  config.vm.provision :ansible do |p|
    # p.verbose = "vvvv"

    p.playbook = "provisioning/playbook.yml"

    p.extra_vars = {
      pwd: Dir.getwd,
      boa: {
        # stable|head
        version: 'stable',
        barracuda: {
          # run barracuda upgrade on provision, after initial build
          upgrade: 'no',
        },
        octopus:{
          # run octopus upgrade on provision, after initial build
          upgrade: 'no',
        },
      },
      git: {
        config: {
          user: [
            { option: 'name',             value: '' },
            { option: 'email',            value: '' },
          ],
        },
      },
      xdebug: {
        ini: [
          { option: 'xdebug.remote_enable',           value: '1' },
          { option: 'xdebug.remote_connect_back',     value: '0' },
          { option: 'xdebug.remote_host',             value: HOST_IP },
          { option: 'xdebug.remote_port',             value: '9000' },
          { option: 'xdebug.remote_handler',          value: 'dbgp' },
          { option: 'xdebug.profiler_enable',         value: '0' },
          { option: 'xdebug.profiler_enable_trigger', value: '1' },
          { option: 'xdebug.max_nesting_level',       value: '200' },
        ],
      },
      # debug python (e.g. pull script) using http://www.jetbrains.com/pycharm/
      pycharm: {
        enable: 'no',
        egg: '/Applications/PyCharm.app/pycharm-debug.egg',
      },
      # debug python (e.g. pull script) using eclipse and http://pydev.org/
      pydev: {
        enable: 'no',
        plugin: '/Applications/eclipse/plugins/org.python.pydev_3.3.3.201401272249',
      },
      grunt: {
        enable: 'no',
      },
      nodejs: {
        tag: 'v0.10.22',
      },
    }.merge(PROVISIONER_VARS)

  end

end
